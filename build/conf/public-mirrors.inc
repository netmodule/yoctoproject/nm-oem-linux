# Changing internal SRC_URI to public

## Bootloaders
SRC_URI_pn-u-boot-ti33x = "git://gitlab.com/netmodule/bootloader/netmodule-uboot.git;user=git;branch=2016.05/standard/am335x;protocol=ssh;"
SRC_URI_pn-u-boot-armada = "git://gitlab.com/netmodule/bootloader/netmodule-uboot.git;user=git;branch=2017.11/standard/armada-385;protocol=ssh;"
SRC_URI_pn-u-boot-imx8-nmhw23 = "git://gitlab.com/netmodule/bootloader/netmodule-uboot.git;user=git;branch=2018.03/imx/imx8-nmhw23;protocol=ssh"

## Kernel
KERNEL_SRC = "git://gitlab.com/netmodule/kernel/linux-netmodule.git;user=git;branch=5.10/standard/base;protocol=ssh"
SRC_URI_pn-linux-netmodule = "${KERNEL_SRC}"
SRC_URI_pn-linux-netmodule-initramfs = "${KERNEL_SRC}"

KERNEL_HW23_SRC = "git://gitlab.com/netmodule/kernel/linux-netmodule.git;user=git;branch=4.14/nxp/nmhw23;protocol=ssh"
SRC_URI_pn-linux-netmodule_imx8-nmhw23 = "${KERNEL_HW23_SRC}"
SRC_URI_pn-linux-netmodule-initramfs_imx8-nmhw23 = "${KERNEL_HW23_SRC}"

# tools
SRC_URI_pn-sys-mon = "git://gitlab.com/netmodule/tools/sys-mon.git;user=git;branch=develop;protocol=ssh"
SRC_URI_pn-sys-mon-native = "git://gitlab.com/netmodule/tools/sys-mon.git;user=git;branch=develop;protocol=ssh"
SRC_URI_pn-nativesdk-sys-mon = "git://gitlab.com/netmodule/tools/sys-mon.git;user=git;branch=develop;protocol=ssh"
SRC_URI_pn-libnmapp = "git://gitlab.com/netmodule/tools/libnmapp.git;user=git;branch=develop;protocol=ssh"
SRC_URI_pn-libnmapp-native = "git://gitlab.com/netmodule/tools/libnmapp.git;user=git;branch=develop;protocol=ssh"
SRC_URI_pn-nativesdk-libnmapp = "git://gitlab.com/netmodule/tools/libnmapp.git;user=git;branch=develop;protocol=ssh"

# Removing AUTOREV from uneeded recipes pointing to internal repos
## distro layer
SRCREV_pn-battery-test = "0"

## bsp layer
SRCREV_pn-dt-overlay = "0"

