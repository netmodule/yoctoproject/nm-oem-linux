# NetModule OEM Linux
The purpose of this repository is to provide a base or a template for customers to build their own
distribution based on the NetModule OEM Linux distribution.

The submodules and source revisions are frozen to match the latest release.


## Initial steps
Before building anything, it is required to get all the submodules. This is done with the following
commands:
```
git submodule init
git submodule update
```

## Selecting hardware target
The hardware target can be selected by sourcing the interactive-machine-selector script:
```
source ./env/interactive-machine-selector
9  # Selecting NG800
```

## Building an image
This distribution provides three image types:

1. The minimal image
2. The "release" image
3. The development image

### The minimal image
It is a initramfs image which can be loaded from the bootloader. It gives access to basic linux
functionalities and allows to flash other images.

You can build it with the following commands:
```
source ./env/distro/minimal-image
bitbake virtual/netmodule-image
```

### The "release" image
This is the default image providing the functionalities of the system.

You can build it with the following commands:
```
source ./env/distro/ostree-image
bitbake netmodule-linux-image
```

### The development image
This is an extended version of the "release" image containing development and debugging tools.

You can build it with the following commands:
```
source ./env/distro/ostree-image
bitbake netmodule-linux-image-dev
```

